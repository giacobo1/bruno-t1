#ifndef PRODUCT_HELPERS_H
#define PRODUCT_HELPERS_H

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <limits>

static unsigned int HandleMatrixInput(char const* filename , std::vector< std::vector<float> > &a_){

	std::string line = "";	
	std::ifstream file_input(filename);		// Poderia ser um try/catch

	int col = 0;
	int row = 0;

	if ( file_input.is_open() ) {
		
		getline(file_input, line);
		
		std::stringstream(line) >> col >> row;
		
		// Se estiver correto o arquivo...
		if ( (col + row) ) {

			float tmp = 0.0;

			// Constructs A from file;
			for ( int i = 0; i < row; i++ ) {
				a_.push_back( *(new std::vector<float>(col)) );
				for ( int j = 0; j < col; j++ ) {
					file_input >> tmp;
					a_[i][j] = tmp;
				}
			}			
		}

		file_input.close();

		return a_.size();

	} else {

		printf("unable to open file\n");
	} 

	return 0;
}


static unsigned int HandleVectorInput(char const* filename , std::vector<float> &u_) {

	std::string line = "";	
	
	std::ifstream file_input(filename);		// Poderia ser um try/catch

	int length = 0;
	
	if ( file_input.is_open() ) {
		
		getline(file_input, line);
		
		std::stringstream(line) >> length;
		
		// Se estiver correto o arquivo...
		if ( length ) {

			float tmp = 0.0;

			// Constructs U from file;
			for ( int i = 0; i < length; i++ ) {
				file_input >> tmp;
				u_.push_back(tmp);
			}
			
		}

		file_input.close();

		return u_.size();

	} else {

		printf("unable to open file\n");
		
	} 

	return 0;
}

static void printVector(std::vector<float>& u) {
	for (unsigned int i = 0; i < u.size(); i++ ) {
			printf("%.2f\n", u[i]);
	}
	putchar('\n');
}

static void printMatrix(std::vector< std::vector<float> >& a) {
	for (unsigned int i = 0; i < a.size(); i++) {
		for (unsigned int j = 0; j < a[i].size(); j++) {
			printf("%.2f ", a[i][j]);
		}
		putchar('\n');
	}
	putchar('\n');
}

#endif