#include "../include/product-helpers.h"
#include <thread>

//#define DEBUGG 

/**
	product: Matrix-Vector Product
	
	Given a matrix A, a vector V, and an assumed solution X to the equation AX=V, this module calculates the actual product AX=V', 
	and then finds the magnitude of the error.

	Inputs

	matrix: the real matrix A.
	actual: the real vector V.
	candidate: the real vector X.
	Outputs

	e: the largest absolute value in the element-wise difference of V and V'.
*/

// NOTE: Cilk possui operadores flexiveis de reducao -- cilk::reducer< cilk::op_max<T> > var;
// @docs: https://www.cilkplus.org/tutorial-reducer-max

// TODO: [product]: Nao estou considerando/tratando a condicao para poder multiplicar matrix-vetor (linaxcoluna) [tratar]

/*
	compile: g++ -Wall src/product-omp.cpp -fopenmp -o bin/product_omp
	run: ./bin/product_omp Input/A_matrix.txt Input/V_vector.txt Input/X_vector.txt 
*/

class SplitMultiplier {

public:
	void operator () () {

		unsigned int size = x_.size();
		// poderia ser em paralelo
		for (unsigned int i = 0; i < size; ++i ) {
			result += x_[i] * a_[line][i];
		}
	}

	SplitMultiplier(std::vector< std::vector<float> > &m, std::vector<float> &u, int index = 0): a_(m), x_(u), line(index) {}
	~SplitMultiplier(){}

	int line;
	float result;
	std::vector<float> &x_;
	std::vector< std::vector<float> > &a_;
};


class MaxFinder {

public:

	void operator() () {

		SplitMultiplier tmp(a_, x_, line);
		
		std::thread run_multiplication(std::ref(tmp));
		run_multiplication.join();

		float diff_abs = abs(v_[line] - tmp.result);

			// aqui deveria ser compartilhado, caso o espaco do for fosse divido
		if (diff_abs > max_diff) {
			max_diff = diff_abs;
		}
	}

	MaxFinder(std::vector< std::vector<float> > &m, std::vector<float> &u, std::vector<float> &k, int index = 0 ):a_(m), x_(u), v_(k) {
		a_size   = a_.size();
		x_size   = x_.size();
		max_diff = 0.0;
		line = index;
	}

	float max_diff;
	int line;
	unsigned int a_size, x_size;
	std::vector<float> &v_;
	std::vector<float> &x_;
	std::vector< std::vector<float> > &a_;

};

int main(int argc, char const *argv[]) {
	
	if ( argc == 4 ) {

		std::vector<float> X;
		std::vector<float> V_0;	
		std::vector< std::vector<float> > A;		

		std::thread read_matrix(HandleMatrixInput, argv[1], std::ref(A));
		std::thread read_X(HandleVectorInput, argv[2], std::ref(X));
		std::thread read_V(HandleVectorInput, argv[3], std::ref(V_0));

		read_matrix.join();
		read_X.join();
		read_V.join();

		//std::cout << std::thread::hardware_concurrency() << std::endl;

		unsigned int size = A.size();

		std::vector< std::thread > workers;
		std::vector< MaxFinder > finders;

		float max_diff = std::numeric_limits<float>::min();

		for (unsigned int i = 0; i < size; i++) {
			finders.push_back(*(new MaxFinder(A, X, V_0, i)));
			workers.push_back(std::thread(std::ref(finders[i])));
		}

		for (unsigned int i = 0; i < size; i++) {
			workers[i].join();

			if (finders[i].max_diff > max_diff) {
				max_diff = finders[i].max_diff;
			}
		}

		printf("%.2f\n", max_diff );

		//printMatrix(matrix_a.A);

		#ifdef DEBUGG
			printMatrix(A);
			printVector(X);
			printVector(V_0);
		#endif

	} else {
		printf("Erro na passagem de parametros.\n");
		exit(1);
	}

	return 0;
}