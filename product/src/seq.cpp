#include <vector>
#include "../include/product-helpers.h"

//#define DEBUGG 

/**
	product: Matrix-Vector Product
	
	Given a matrix A, a vector V, and an assumed solution X to the equation AX=V, this module calculates the actual product AX=V', 
	and then finds the magnitude of the error.

	Inputs

	matrix: the real matrix A.
	actual: the real vector V.
	candidate: the real vector X.
	Outputs

	e: the largest absolute value in the element-wise difference of V and V'.
*/


std::vector<float> MultiplyMatrixVector(std::vector< std::vector<float> > &a_, std::vector<float> &x_) {
	std::vector<float> ret;

	unsigned int row = a_.size();

	float tmp = 0.0;

	for ( unsigned int i = 0; i < row; i++ ) {
		for ( unsigned int j = 0; j < a_[i].size(); j++ ) {
			tmp += x_[j] * a_[i][j];
		}

		ret.push_back(tmp);
		tmp = 0.0;
	}

	return ret;

}

float findMaxDiff(std::vector<float> &v_0, std::vector<float> &v_1) {

	unsigned int size = v_0.size();

	float max = std::numeric_limits<float>::min();

	for (unsigned int i = 0; i < size; i++ ) {

		float diff_abs = abs(v_0[i] - v_1[i]);

		if ( diff_abs > max ) {
			max = diff_abs;
		}
	}

	return max;
}

int main(int argc, char const *argv[]) {

	
	if ( argc == 4 ) {

		std::vector<float> X;
		std::vector<float> V_0;	
		std::vector<float> V_1;	
		std::vector< std::vector<float> > A;

		HandleMatrixInput(argv[1], A);
		HandleVectorInput(argv[2], X);
		HandleVectorInput(argv[3], V_0);		

		V_1 = MultiplyMatrixVector(A, X);

		printf("%.2f\n", findMaxDiff(V_0, V_1));

		#ifdef DEBUGG
			printMatrix(A);
			printVector(X);
			printVector(V_0);
			printVector(V_1);
		#endif

	} else {
		printf("Erro na passagem de parametros.\n");
		exit(1);
	}

	return 0;
}