#include <vector>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <cilk/reducer_max.h>
#include <cilk/reducer_opadd.h>
#include "../include/product-helpers.h"

//#define DEBUGG 

/**
	product: Matrix-Vector Product
	
	Given a matrix A, a vector V, and an assumed solution X to the equation AX=V, this module calculates the actual product AX=V', 
	and then finds the magnitude of the error.

	Inputs

	matrix: the real matrix A.
	actual: the real vector V.
	candidate: the real vector X.
	Outputs

	e: the largest absolute value in the element-wise difference of V and V'.
*/

// NOTE: Cilk possui operadores flexiveis de reducao -- cilk::reducer< cilk::op_max<T> > var;
// @docs: https://www.cilkplus.org/tutorial-reducer-max

// TODO: [product]: Nao estou considerando/tratando a condicao para poder multiplicar matrix-vetor (linaxcoluna) [tratar]

float MultiplyMatrixVector(std::vector< std::vector<float> > &a_, std::vector<float> &x_, int r,unsigned int s) {
	
	cilk::reducer< cilk::op_add<float> > tmp(0.0);

	cilk_for (unsigned int i = 0; i < s; i++) *tmp += x_[i] * a_[r][i];
	
	return tmp.get_value();
}

float findMax( std::vector< std::vector<float> > &a_, std::vector<float> &x_, std::vector<float> &v_a) {
	
	unsigned int x_size = x_.size();
	unsigned int a_size = a_.size();

	std::vector<float> v_b;

	cilk::reducer< cilk::op_max<float> > max_diff;

	for ( int i = 0; i < x_size; i++ ) v_b.push_back( abs(v_a[i] - MultiplyMatrixVector(a_, x_, i, x_size) ));

	cilk_for (int i = 0; i < a_size; i++) max_diff->calc_max(v_b[i]);

	return max_diff.get_value();
}
/*
	compile: g++ -Wall src/product-omp.cpp -fopenmp -o bin/product_omp
	run: ./bin/product_omp Input/A_matrix.txt Input/V_vector.txt Input/X_vector.txt 
*/

int main(int argc, char const *argv[]) {
	
	if ( argc == 4 ) {

		std::vector<float> X;
		std::vector<float> V_0;	
		std::vector< std::vector<float> > A;		

		unsigned int A_size = cilk_spawn HandleMatrixInput(argv[1], A);
		unsigned int X_size = cilk_spawn HandleVectorInput(argv[2], X);
		HandleVectorInput(argv[3], V_0);

		cilk_sync;

		float max_diff = findMax(A, X, V_0);

		printf("%.2f\n", max_diff);

		#ifdef DEBUGG
			printMatrix(A);
			printVector(X);
			printVector(V_0);
		#endif

	} else {
		printf("Erro na passagem de parametros.\n");
		exit(1);
	}

	return 0;
}