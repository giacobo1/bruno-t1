#include <vector>
#include "../include/product-helpers.h"

//#define DEBUGG 

/**
	product: Matrix-Vector Product
	
	Given a matrix A, a vector V, and an assumed solution X to the equation AX=V, this module calculates the actual product AX=V', 
	and then finds the magnitude of the error.

	Inputs

	matrix: the real matrix A.
	actual: the real vector V.
	candidate: the real vector X.
	Outputs

	e: the largest absolute value in the element-wise difference of V and V'.
*/


// TODO [product]: Nao estou considerando/tratando a condicao para poder multiplicar matrix-vetor (linaxcoluna) [tratar]

float MultiplyMatrixVector(std::vector< std::vector<float> > &a_, std::vector<float> &x_, int r,unsigned int s) {

	float tmp = 0.0;

	#pragma omp parallel for reduction(+:tmp) shared(a_, x_, r, s)
		for (unsigned int j = 0; j < s; j++ ) {
			tmp += x_[j] * a_[r][j];
		}

	return tmp;
}


/*
	compile: g++ -Wall src/product-omp.cpp -fopenmp -o bin/product_omp
	run: ./bin/product_omp Input/A_matrix.txt Input/V_vector.txt Input/X_vector.txt 
*/

int main(int argc, char const *argv[]) {
	
	if ( argc == 4 ) {

		std::vector<float> X;
		std::vector<float> V_0;	
		std::vector< std::vector<float> > A;

		unsigned int A_size = 0;
		unsigned int X_size = 0;

		float max_diff = std::numeric_limits<float>::min();

		#pragma omp parallel shared(A, X, V_0, A_size, X_size, max_diff)
		{
			// Load data concurrently;
			#pragma omp sections
			{
				#pragma omp section
					A_size = HandleMatrixInput(argv[1], A);					

				#pragma omp section
					X_size = HandleVectorInput(argv[2], X);
				
				#pragma omp section
					HandleVectorInput(argv[3], V_0);
			}

			#pragma omp for schedule(static) nowait
				for (unsigned int i = 0; i < A_size; i++) {

					float diff_abs = abs(V_0[i] - MultiplyMatrixVector(A, X, i, X_size));

					#pragma omp critical
					{
						if ( diff_abs > max_diff ) {
							max_diff = diff_abs;
						}
					}								
				}
				
		}

		printf("%.2f\n", max_diff);

		#ifdef DEBUGG
			printMatrix(A);
			printVector(X);
			printVector(V_0);
		#endif

	} else {
		printf("Erro na passagem de parametros.\n");
		exit(1);
	}

	return 0;
}