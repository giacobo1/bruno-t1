//#include "../include/product-helpers.h"
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <limits>
#include <tbb/tbb.h>


//#define DEBUGG 

/**
	product: Matrix-Vector Product
	
	Given a matrix A, a vector V, and an assumed solution X to the equation AX=V, this module calculates the actual product AX=V', 
	and then finds the magnitude of the error.

	Inputs

	matrix: the real matrix A.
	actual: the real vector V.
	candidate: the real vector X.
	Outputs

	e: the largest absolute value in the element-wise difference of V and V'.
*/


// TODO [product]: Nao estou considerando/tratando a condicao para poder multiplicar matrix-vetor (lina x coluna) [tratar]
// TODO: Atualmente, estou compilando sem a flag -Wall, muitos warnings estao sendo omitidos, apos finalizar implementacoes
//		 tratar os warnings.
// TODO: Tratar limites dos vetores nos metodos que os acessam.
// TODO: Trocar os nomes das classes de input, arrumar melhor o codigo, testar mais, documentar
// TODO: Nao sei se precisa usar o task_scheduler_init [ver]

class ReduceMultiplier {

public:
	void operator () ( const tbb::blocked_range<int>&r ) {
		for ( int i = r.begin(); i != r.end(); ++i ) {
			result += x_[i] * a_[line][i];
		}
	}

	void join( const ReduceMultiplier& m ){ result += m.result; }

	ReduceMultiplier(std::vector< std::vector<float> > &m, std::vector<float> &u, int index = 0): a_(m), x_(u), line(index) {}
	
	// Bizzarrices do C++ - inicializacao de membros referencia via construtor de copia obrigatoriamente
	ReduceMultiplier( const ReduceMultiplier& r , tbb::split):a_(r.a_), x_(r.x_) {

		line = r.line;
		result = r.result;
	}

	~ReduceMultiplier(){}

	int line;
	float result;

	std::vector<float> &x_;
	std::vector< std::vector<float> > &a_;

};

class MaxFinder {

public:
	void operator () ( const tbb::blocked_range<int>&r ) {
		for ( int i = r.begin(); i != r.end(); ++i ) {

			ReduceMultiplier *tmp = new ReduceMultiplier(a_, x_, i);

			tbb::parallel_reduce(tbb::blocked_range<int>(0, x_size), *tmp );

			float diff_abs = abs(v_[i] - tmp->result);

			if (diff_abs > max_diff) {
				max_diff = diff_abs;
			}

			delete tmp;
		}
	}

	// Consegui tomar vantagem da integracao com a linguagem e fazer uma reducao sobre outra
	void join( const MaxFinder& m ) {

		if (m.max_diff > max_diff) {
				max_diff = m.max_diff;
		}
	}

	MaxFinder( std::vector< std::vector<float> > &m, std::vector<float> &u, std::vector<float> &k, float d = 0.0)\
	:a_(m), x_(u), v_(k), max_diff(d){
		a_size = a_.size();
		x_size = x_.size();
		max_diff = std::numeric_limits<float>::min();
	}

	MaxFinder( const MaxFinder& m , tbb::split) :a_(m.a_), x_(m.x_), v_(m.v_){
		
		max_diff = m.max_diff;
		a_size   = m.a_size;
		x_size   = m.x_size;		
	}

	~MaxFinder(){}

	float max_diff;
	unsigned int a_size, x_size;
	std::vector<float> &v_;
	std::vector<float> &x_;
	std::vector< std::vector<float> > &a_;

};

// trocar o nome destas classes
class HandleMatrixInput : public tbb::task {

public:
	HandleMatrixInput( const char* fname, std::vector< std::vector<float> > &x):A(x) {

		a_size = 0;
		file_input.open(fname, std::ifstream::in);
	}
	~HandleMatrixInput(){}

	tbb::task* execute() {

		std::string line = "";

		int col = 0;
		int row = 0;

		if ( file_input.is_open() ) {			

			getline(file_input, line);
			
			std::stringstream(line) >> col >> row;
				
			// Se estiver correto o arquivo...
			if ( (col + row) ) {

				float tmp = 0.0; 

				// Constructs A from file;
				for ( int i = 0; i < row; i++ ) {
					A.push_back( *(new std::vector<float> (col)) );
					for (int j = 0; j < col; j++){
						file_input >> tmp;
						A[i][j] = tmp;
									
					}					
				}

				a_size = A.size();
				file_input.close();
							
			} else {
				printf("unable to open file\n");
			}	
		} 

		return NULL;
	}

	unsigned int a_size;
	std::ifstream file_input;
	std::vector< std::vector<float> > &A;

};


class HandleVectorInput : public tbb::task {

public:

	HandleVectorInput( const char* fname, std::vector<float> &u):v_(u) {

		v_size = 0;
		file_input.open(fname, std::ifstream::in);
	}

	~HandleVectorInput(){}

	tbb::task* execute() {
		
		std::string line = "";

		int length = 0;
	
		if ( file_input.is_open() ) {
		
			getline(file_input, line);
			
			std::stringstream(line) >> length;
			
			// Se estiver correto o arquivo...
			if ( length ) {

				float tmp = 0.0;

				// Constructs U from file;
				for ( int i = 0; i < length; i++ ) {
					file_input >> tmp;
					v_.push_back(tmp);
				}

				v_size = v_.size();			
			}

			file_input.close();
							
		} else {
				printf("unable to open file\n");
		}

		return NULL;
	}

	unsigned int v_size;
	std::ifstream file_input;
	std::vector<float> &v_;

};




void printMatrix(std::vector< std::vector<float> > &a) {
	for (unsigned int i = 0; i < a.size(); i++) {
		for (unsigned int j = 0; j < a[i].size(); j++) {
			printf("%.2f ", a[i][j]);
		}
		putchar('\n');
	}
	putchar('\n');
}


void printVector(std::vector<float>& u) {
	for (unsigned int i = 0; i < u.size(); i++ ) {
			printf("%.2f\n", u[i]);
	}
	putchar('\n');
}

/*
	compile: g++ -Wall src/product-tbb.cpp -o bin/product_tbb -ltbb -lm
	run: ./bin/product_tbb Input/A_matrix.txt Input/X_vector.txt Input/V_vector.txt | <----- A ordem importa
*/

int main(int argc, char const *argv[]) {
	
	if ( argc == 4 ) {
		
		HandleVectorInput &X = *new(tbb::task::allocate_root()) HandleVectorInput(argv[2], *(new std::vector<float>()));
		HandleVectorInput &V_0 = *new(tbb::task::allocate_root()) HandleVectorInput(argv[3], *(new std::vector<float>()));
		HandleMatrixInput &matrix_a = *new(tbb::task::allocate_root()) HandleMatrixInput(argv[1], *(new std::vector< std::vector<float> > ()));

		tbb::task::spawn_root_and_wait(X);
		tbb::task::spawn_root_and_wait(V_0); 
		tbb::task::spawn_root_and_wait(matrix_a);

		MaxFinder f(matrix_a.A, X.v_, V_0.v_);

		int size = (int) matrix_a.a_size;

		tbb::parallel_reduce(tbb::blocked_range<int>(0, size), f );

		printf("%.2f\n", f.max_diff);

		#ifdef DEBUGG
			printMatrix(matrix_a.A);
			printVector(X.v_);
			printVector(V_0.v_);
		#endif		

	} else {
		printf("Erro na passagem de parametros.\n");
		exit(1);
	}

	return 0;
}