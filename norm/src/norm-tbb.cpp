/**

norm: Point Location Normalization
This module normalizes point coordinates so that all points lie within the unit square [0..1]×[0..1]. 
If xmin and xmax are the minimum and maximum x coordinate values in the input vector, 
then the normalization equation is

xi'	=	(xi — xmin)/(xmax — xmin)
y coordinates are normalized in the same fashion.

Inputs

points: a vector of point locations.
Outputs

points: a vector of normalized point locations.

*/

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <tbb/tbb.h>
#include "../../common/point.h"

// COMPILE: icpc -Wall src/norm-tbb.cpp -o bin/norm_tbb -ltbb

class Normalizer{

	float dx, dy;
	float mx, my;
	std::vector< Point::Point<float> > pv;

public:
	void operator () ( const tbb::blocked_range<int>&r ) const {
		for ( int i = r.begin(); i != r.end(); ++i ) {
			printf("%.3f %.3f\n", (pv[i].x_ - mx)/dx,  (pv[i].y_ - my)/dy);
		}
	}
 
	Normalizer(std::vector< Point::Point<float> >& p, float dx_, float dy_, float mx_, float my_ ): pv(p), dx(dx_), dy(dy_), mx(mx_), my(my_) {}
};


/**
	Implementacao sequencial da normalizacao de vetores

	In: um vetor de pontos (x,y) -- LIDOS EM STDIN
	Return: um vetor de pontos normalizados [0..1]x[0..1] -- ENVIADOS PARA STDOUT

*/
int main( int argc, char *argv[] ) {

	using namespace std;

	// Vetor que sera normalizado
	std::vector< Point::Point<float> > point_vector;

	string stdinput = "";

	float tmp_x = 0.0;
	float tmp_y = 0.0;

	float delta_x = 0.0;
	float delta_y = 0.0;

	// Valores de referencia para comparacao do min/max
	float max_x = std::numeric_limits<float>::min();
	float min_x = std::numeric_limits<float>::max();
	float max_y = std::numeric_limits<float>::min();
	float min_y = std::numeric_limits<float>::max();


	// Le o conjunto de pontos da entrada padrao, armazena no veto e descobre os (x/y)(max/min)
	while ( getline(cin, stdinput ) ) {
		
		stringstream(stdinput) >> tmp_x >> tmp_y;
		point_vector.push_back(*(new Point::Point<float>( tmp_x , tmp_y )));

		if ( max_x < tmp_x ) {

			max_x = tmp_x;
		}

		if ( min_x > tmp_x ) {

			min_x = tmp_x;
		}

		if ( max_y < tmp_y ) {

			max_y = tmp_y;
		}

		if ( min_y > tmp_y ) {

			min_y = tmp_y;
		}		

	}

	// Calcula os deltas a priore, evitando recalculos
	delta_x = (max_x - min_x);
	delta_y = (max_y - min_y);

	int size  = point_vector.size();
		
	Normalizer N(point_vector, delta_x, delta_y, min_x, min_y);

	tbb::parallel_for (tbb::blocked_range<int>(0, size), N);

	return EXIT_SUCCESS;
}