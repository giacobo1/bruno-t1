/**

norm: Point Location Normalization
This module normalizes point coordinates so that all points lie within the unit square [0..1]×[0..1]. 
If xmin and xmax are the minimum and maximum x coordinate values in the input vector, 
then the normalization equation is

xi'	=	(xi — xmin)/(xmax — xmin)
y coordinates are normalized in the same fashion.

Inputs

points: a vector of point locations.
Outputs

points: a vector of normalized point locations.

*/

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include "../../common/point.h"


// Exemplo legal sobre como pegar o tempo abaixo
// @docs: http://www.cilkplus.org/fibcpp -- example
// @docs: https://www.cilkplus.org/docs/doxygen/include-dir/cilk__api_8h.html -- cilk docs

// TODO: Adicionar pasta de inputs e um input 'default'

void NormalizeVector(std::vector< Point::Point<float> >& pv, int index, float dx, float dy, float mx, float my) {
	printf("%.3f %.3f\n", (pv[index].x_ - mx)/dx,  (pv[index].y_ - my)/dy);
}


/**
	Implementacao sequencial da normalizacao de vetores

	In: um vetor de pontos (x,y) -- LIDOS EM STDIN
	Return: um vetor de pontos normalizados [0..1]x[0..1] -- ENVIADOS PARA STDOUT

*/
int main( int argc, char *argv[] ) {

	using namespace std;

	// Vetor que sera normalizado
	std::vector< Point::Point<float> > point_vector;

	string stdinput = "";

	float tmp_x = 0.0;
	float tmp_y = 0.0;

	float delta_x = 0.0;
	float delta_y = 0.0;

	// Valores de referencia para comparacao do min/max
	float max_x = std::numeric_limits<float>::min();
	float min_x = std::numeric_limits<float>::max();
	float max_y = std::numeric_limits<float>::min();
	float min_y = std::numeric_limits<float>::max();


	// Le o conjunto de pontos da entrada padrao, armazena no veto e descobre os (x/y)(max/min)
	while ( getline(cin, stdinput ) ) {
		
		stringstream(stdinput) >> tmp_x >> tmp_y;
		point_vector.push_back(*(new Point::Point<float>( tmp_x , tmp_y )));

		if ( max_x < tmp_x ) {

			max_x = tmp_x;
		}

		if ( min_x > tmp_x ) {

			min_x = tmp_x;
		}

		if ( max_y < tmp_y ) {

			max_y = tmp_y;
		}

		if ( min_y > tmp_y ) {

			min_y = tmp_y;
		}		

	}

	//printf("# Workers: %d\n", __cilkrts_get_nworkers());

	// Calcula os deltas a priore, evitando recalculos
	delta_x = (max_x - min_x);
	delta_y = (max_y - min_y);

	int size  = point_vector.size();
		
	cilk_for ( int i = 0; i < size; ++i ) NormalizeVector(point_vector, i, delta_x, delta_y, min_x, min_y);


	return EXIT_SUCCESS;
}