#ifndef POINT_H
#define POINT_H


// NOTE: Possibilidade de implementar um ou mais metodos que manipulem pontos. (i.e. distancia, soma);

namespace Point{

/**
	Classe que representa pontos no plano cartesiano.
*/


template <typename T> class Point {

public:
	T x_, y_;

	Point( T x = 0, T y = 0 ) : x_(x), y_(y){}
	
	Point( const Point<T>& p ) {
		this->x_ = p.x_;
		this->y_ = p.y_;
	}

	void operator = ( const Point<T>& p ) {
		this->x_ = p.x_;
		this->y_ = p.y_;
	}

	friend Point<T>* operator + ( const Point<T>& a, const Point<T>& b ){
		return new Point<T>(( a.x_ + b.x_ ), ( a.y_ + b.y_ ));
	} 


	friend Point<T>* operator - ( const Point<T>& a, const Point<T>& b  ){
		return new Point<T>(( a.x_ - b.x_ ), ( a.y_ - b.y_ ));
	}

	~Point(void){};
};

}
#endif