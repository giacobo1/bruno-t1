-- Algumas diretivas para compilar (icpc)
	icc -Wall -O3 lu_omp.c utils.c -o lu-omp -fopenmp
	icpc -Wall -O3 ../anahy/*.o lu_a3.cpp utils.c -o lu-a3 -lpthread
	icpc -Wall -O3 lu_cilk.cpp utils.c -o lu-cilk
	icpc -Wall -O3 lu_tbb.cpp utils.c -o lu-tbb -ltbb
	icc -Wall -O3 lu_seq.c utils.c -o lu-seq

-- TODO [Geral]   : Ainda não adicionado -O3 nas compilações;
-- TODO [Geral]   : Criar Makefiles para todos;
-- TODO [norm]    : Criar diretorio Input com um 'input-default.txt';
-- TODO [product] : verificar divisao do trabalho, ver comentarios;
-- TODO [product] : Verificação das dimensões de matrizes/vetores não tratado
